public class Main{
    public static void main(String[] args) {
        Wrapper<String> myClass = new Wrapper<>();
        myClass.setItem("hello");
        myClass.setItem("home");
        myClass.setItem(null);
        myClass.setItem("hi");
        myClass.setItem("world");
        myClass.setItem("hi");
        myClass.setItem("HRD");
        for (int i = 0; i < myClass.size(); i++) {
            System.out.println(myClass.getItem(i));
        }
    }
}
