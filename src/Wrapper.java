import java.util.Arrays;
public class Wrapper<E> {
    private int size = 0;
    private static final int DEFAULT_CAPACITY = 10;
    private Object[] obj;

    public Wrapper() {
        obj = new Object[DEFAULT_CAPACITY];
    }
    public void setItem(E e){
        if (size == obj.length) {
            ensureCapacity();
        }
        try {
            if (e == null) {
                throw new NullValueException("Cannot input null value");
            }
            for (Object o : obj) {
                if (o == e) {
                    throw new DuplicateValueException("Cannot input duplicate value");
                }
            }
            this.obj[size++] = e;
        }catch (NullValueException| DuplicateValueException err){
            System.out.println(err);
        }
    }
    public Object getItem(int i) {
        if (i >= size || i < 0) {
            throw new IndexOutOfBoundsException("Index: " + i + ", Size " + i);
        }
        return obj[i];
    }
    public int size() {
        return size;
    }
    private void ensureCapacity() {
        int newSize = obj.length + 1;
        obj = Arrays.copyOf(obj, newSize);
    }
}
class NullValueException extends Exception{
    NullValueException(String s){
        super(s);
    }
}
class DuplicateValueException extends Exception{
    DuplicateValueException(String s){
        super(s);
    }
}



